import json
import os
from functools import reduce  # forward compatibility for Python 3
import operator

def parenthesis_split(s, separators=' \n'):
    l = []
    p = 0
    expr = ''
    for c in s.strip():
        if c == '(' or c == '[':
            p += 1
        if c == ')' or c == ']':
            p -= 1
        if (p == 0) and (c in separators) and not expr.endswith('-'):
            l.append(expr)
            expr = ''
        else:
            expr += c
    else:
        l.append(expr)
    return l


def process_args(args, parent_opts):
    short_flag = False
    for arg in args:
        if (not len(arg)):
            continue

        if (arg == 'COMMAND' or arg == 'GROUP'):
            continue

        if (arg.strip()[0] == '['):
            try:
                if arg.strip()[1] == '[' or arg.strip()[1] == '(':
                    yield list(process_args(arg,  {**parent_opts, 'parentOptional': True}))
                    continue
            except:
                pass

            exclusive_content = parenthesis_split(arg[1:-1], '|')
            conditional_content = parenthesis_split(arg[1:-1], ':')

            if (len(exclusive_content) == 1 and len(conditional_content) == 1):
                if arg.startswith('-') and not arg.startswith('--'):
                    short_flag = True
                    continue

                yield {
                    **get_value_specification(arg[1:-1].strip(' ')),
                    'isOptional': True,
                    'isPositional': not arg[1:-1].startswith('--') or arg[1:-1].startswith('-- '),
                }
                continue

            if (len(exclusive_content) > 1):
                yield list(process_args(exclusive_content,  {**parent_opts, 'parentOptional': True, 'isExclusive': True}))
                continue

            if (len(conditional_content) > 1 and len(conditional_content[1]) == 0):
                if arg.startswith('-') and not arg.startswith('--'):
                    short_flag = True
                    continue

                yield {
                    **get_value_specification(arg[1:-1].strip(' ')),
                    'isOptional': True,
                    'isPositional': not arg[1:-1].startswith('--') or arg[1:-1].startswith('-- '),
                }
                continue

            if (len(conditional_content) > 1):
                yield list(process_args(conditional_content,  {**parent_opts, 'parentOptional': True, 'isExclusive': False}))
                continue

        elif (arg.strip()[0] == '('):
            try:
                if arg.strip()[1] == '[' or arg.strip()[1] == '(':
                    yield list(process_args(arg,  {**parent_opts, 'parentOptional': True}))
                    continue
            except:
                pass

            exclusive_content = parenthesis_split(arg[1:-1], '|')
            conditional_content = parenthesis_split(arg[1:-1], ':')

            if (len(exclusive_content) == 1 and len(conditional_content) == 1):
                if arg.startswith('-') and not arg.startswith('--'):
                    short_flag = True
                    continue

                yield {
                    **get_value_specification(arg[1:-1].strip(' ')),
                    'isPositional': not arg[1:-1].startswith('--') or arg[1:-1].startswith('-- '),
                    **parent_opts,
                }
                continue

            if (len(exclusive_content) > 1):
                yield list(process_args(exclusive_content, {**parent_opts, 'parentOptional': False, 'isExclusive': True}))
                continue

            if (len(conditional_content) > 1 and len(conditional_content[1]) == 0):
                if arg.startswith('-') and not arg.startswith('--'):
                    short_flag = True
                    continue

                yield {
                    **get_value_specification(arg[1:-1].strip(' ')),
                    'isOptional': True,
                    'isPositional': not arg[1:-1].startswith('--') or arg[1:-1].startswith('-- '),
                }
                continue
            if (len(conditional_content) > 1):
                yield list(process_args(conditional_content, {**parent_opts, 'parentOptional': False, 'isExclusive': False}))
                continue

        else:
            if (short_flag):
                short_flag = False
                continue

            exclusive_content = parenthesis_split(arg, '|')
            conditional_content = parenthesis_split(arg, ':')

            if (len(exclusive_content) == 1 and len(conditional_content) == 1):
                if arg.startswith('-') and not arg.startswith('--'):
                    short_flag = True
                    continue

                yield {
                    **get_value_specification(arg.strip(' ')),
                    'isPositional': not arg.startswith('--') or arg.startswith('-- '),
                    **parent_opts,
                }
                continue

            if (len(exclusive_content) > 1):
                yield list(process_args(exclusive_content, {**parent_opts, 'isExclusive': True}))
                continue

            if (len(conditional_content) > 1 and len(conditional_content[1]) == 0):
                if arg.startswith('-') and not arg.startswith('--'):
                    short_flag = True
                    continue

                yield {
                    **get_value_specification(arg[1:-1].strip(' ')),
                    'isOptional': True,
                    'isPositional': not arg[1:-1].startswith('--') or arg[1:-1].startswith('-- '),
                }
                continue

            if (len(conditional_content) > 1):
                yield list(process_args(conditional_content, {**parent_opts, 'isExclusive': False}))
                continue


def get_value_specification(arg):
    args = [a.strip(' ') for a in arg.split(',')]

    value, *valuePlaceholder = args[0].replace('\n', '').split('=', 1)

    expectsValue = bool(valuePlaceholder)

    multiArgument = arg.endswith('…')

    isTrailingArg = value.startswith('-- ')

    trailingSeparator = '--' if isTrailingArg else None

    value = value[3:] if value.startswith('-- ') else value

    return {
        'multiArgument': multiArgument,
        'name': value if not multiArgument else value[:-1].strip(' '),
        'valueSeparator': '=' if expectsValue else '',
        'isTrailingArg': isTrailingArg,
        'trailingSeparator': trailingSeparator,
        'expectsValue': True if not arg.startswith('--') or arg.startswith('-- ') else expectsValue,
        'valuePlaceholder': valuePlaceholder[0] if len(valuePlaceholder) > 0 else arg.strip(' ') if not arg.startswith('--') else None,
    }


with open('scrapped/gcloud/gcloud_instructions.json', 'r') as f:
    data = json.load(f)

parsed_instructions = [
    {
        **instruction,
        'args': list(process_args(parenthesis_split(instruction['args']), {})),
        'rawArgs': instruction['args'],
    }
    for instruction in data if instruction['args']
]

def setInDict(dataDict, mapList, value):
    getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value


def getFromDict(dataDict, mapList):
    return reduce(operator.getitem, mapList, dataDict)


with open('gcloud_global_flags.json', 'r') as f:
    global_flags = json.load(f)

with open('scrapped/gcloud/docs/gcloud_group_docs.json', 'r') as f:
    group_docs = json.load(f)

for instruction in parsed_instructions:
    for i, j in enumerate(instruction['args']):
        try:
            if (j['name'] == 'GCLOUD_WIDE_FLAG'):
                instruction['args'].pop(i)
                instruction['args'].extend(
                    # TODO: Remove 'globalFlag' attribute after version 0.0.8 becomes deprecated
                    [{**flag, 'globalFlag': True, 'isGlobal': True} for flag in global_flags])
                break
        except:
            continue

    reference_path = []

    for command in instruction['commands']:
        reference_path.append(command['value'])
        # print(group_docs)
        try:
            docs = getFromDict(group_docs, [*reference_path])
            command['docs'] = docs['docs_']
        except:
            pass
        # setInDict(command, ['value', 'docs'], docs)


filename = f'processed/gcloud/gcloud_instructions.json'

os.makedirs(os.path.dirname(filename), exist_ok=True)

with open(filename, 'w') as f:
    json.dump(parsed_instructions, f, indent=2)
