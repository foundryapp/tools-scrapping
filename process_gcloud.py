import json
import os
from functools import reduce  # forward compatibility for Python 3
import operator

with open('gcloud_global_flags.json', 'r') as f:
    gcloud_global_flags = json.load(f)

def parse_flag_arg(arg):
    arg = arg.strip().replace('`', '')
    parsed_arg = arg.split('=')

    if (len(parsed_arg) > 1):
        expects_value = True
        value_placeholder = parsed_arg[1]
        value_separator = '='
    else:
        value_placeholder = None
        expects_value = False
        value_separator = None

    return {
        'expectsValue': expects_value,
        'valuePlaceholder': value_placeholder,
        'valueSeparator': value_separator,
        'name': parsed_arg[0],
    }

def parse_positional_arg(arg):
    arg = arg.strip()
    return {
        'name': arg.replace('`', ''),
        'isOptional': False,
        'isPositional': True,
        'valuePlaceholder': arg.replace('`', ''),
        'expectsValue': True,
    }

def assemble_args(args):
    flag_prefix = '`--'

    required_flags = [{**parse_flag_arg(arg), 'isOptional': False} for arg in args['requiredFlags'] if arg.startswith(flag_prefix)]
    global_flags = [{**arg, 'isGlobal': True, 'globalFlag': True} for arg in gcloud_global_flags] if args['globalFlags'] else []
    optional_flags = [{**parse_flag_arg(arg), 'isOptional': True} for arg in args['optionalFlags'] if arg.startswith(flag_prefix)]
    flags = [{**parse_flag_arg(arg), 'isOptional': True} for arg in args['flags'] if arg.startswith(flag_prefix)]
    positional_arguments = [parse_positional_arg(arg) for arg in args['positionalArguments']]

    try:
        other_flags = [{**parse_flag_arg(arg), 'isOptional': True} for arg in args['otherFlags'] if arg.startswith(flag_prefix)]
    except:
        other_flags = []

    try:
        common_flags = [{**parse_flag_arg(arg), 'isOptional': True} for arg in args['commonFlags'] if arg.startswith(flag_prefix)]
    except:
        common_flags = []

    try:
        list_flags = [{**parse_flag_arg(arg), 'isOptional': True} for arg in args['listFlags'] if arg.startswith(flag_prefix)]
    except:
        list_flags = []

    command_flags = [*required_flags, *flags, *optional_flags, *list_flags, *common_flags, *other_flags]

    return [[arg] for arg in [*positional_arguments, *command_flags, *global_flags]]


with open('scrapped/gcloud/gcloud_instructions.json', 'r') as f:
    data = json.load(f)

parsed_instructions = [
    {
        **instruction,
        'args': assemble_args(instruction['args']),
    }
    for instruction in data if instruction['args']
]


def setInDict(dataDict, mapList, value):
    getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value


def getFromDict(dataDict, mapList):
    return reduce(operator.getitem, mapList, dataDict)



with open('scrapped/gcloud/docs/gcloud_group_docs.json', 'r') as f:
    group_docs = json.load(f)

for instruction in parsed_instructions:
    reference_path = []

    for command in instruction['commands']:
        reference_path.append(command['value'])
        # print(group_docs)
        try:
            docs = getFromDict(group_docs, [*reference_path])
            command['docs'] = docs['docs_']
        except:
            pass
        # setInDict(command, ['value', 'docs'], docs)


filename = f'processed/gcloud/gcloud_instructions.json'

os.makedirs(os.path.dirname(filename), exist_ok=True)

with open(filename, 'w') as f:
    json.dump(parsed_instructions, f, indent=2)
