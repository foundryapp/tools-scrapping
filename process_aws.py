import json
import os


def parse_arg(raw_arg):
    is_optional = False

    args = []

    if raw_arg.startswith('['):
        is_optional = True

    trimmed = raw_arg[1: len(raw_arg) -
                      2].strip() if is_optional else raw_arg

    for arg in trimmed.split('|'):
        expects_value = arg.endswith('>')
        name = arg[0:arg.find('<')].strip() if expects_value else arg

        args.append({
            'name': name,
            'isOptional': is_optional,
            'expectsValue': expects_value,
        })

    return args


with open('scrapped/aws/aws_instructions.json', 'r') as f:
    scrapped_instructions = json.load(f)

with open('aws_global_flags.json', 'r') as f:
    global_flags = json.load(f)


processed_instructions = [
    {
        **instruction,
        'args': [*[parse_arg(arg) for arg in instruction['args']],
                 *[[{**flag, 'globalFlag': True, 'isGlobal': True}] for flag in global_flags]],
    }
    for instruction in scrapped_instructions
]


filename = f'processed/aws/aws_instructions.json'

os.makedirs(os.path.dirname(filename), exist_ok=True)

with open(filename, 'w') as f:
    json.dump(processed_instructions, f, indent=2)
