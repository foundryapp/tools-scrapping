from scrapy.crawler import CrawlerProcess
import scrapy
from bs4 import BeautifulSoup
import json
import time
import os
from string import Template
import html2text

h = html2text.HTML2Text()
h.ignore_links = True

aws_root = 'https://awscli.amazonaws.com/v2/documentation/api/latest/reference/index.html'
aws_command_template = Template(
    'https://awscli.amazonaws.com/v2/documentation/api/latest/reference/${command}/index.html')
aws_subcommand_template = Template(
    'https://awscli.amazonaws.com/v2/documentation/api/latest/reference/${command}/${subcommand}.html')

data = []


class CLIScrapper(scrapy.Spider):
    name = 'aws'

    def start_requests(self):
        urls = [aws_root]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_root)

    def parse_root(self, response):
        soup = BeautifulSoup(response.text, 'html.parser')

        commands = [
            list(a.children)[0]
            for a in soup
            .find(id='available-services')
            .find_all('a')
        ]

        del commands[0]

        # Uncomment this if you want to print a formatted dict of all services, with basic splitted keywords
        # d = {f'{command}': [command, *
        #                     splitter.split(command)] for command in commands}
        # print(d)
        # return

        for command in commands:
            url = aws_command_template.substitute({'command': command})
            yield scrapy.Request(
                url=url,
                callback=self.parse_command,
                meta={'command': command}
            )

    def parse_command(self, response):
        command = response.meta.get('command')

        soup = BeautifulSoup(response.text, 'html.parser')

        subcommands = [
            list(a.children)[0]
            for a in soup
            .find(id='available-commands')
            .find_all('a')
        ]

        del subcommands[0]


        try:
            soup.find(id='available-commands').decompose()
            command_docs = h.handle(str(soup.find(id=command)))
        except:
            command_docs = ''

        for subcommand in subcommands:
            url = aws_subcommand_template.substitute(
                {'command': command, 'subcommand': subcommand})

            yield scrapy.Request(
                url=url,
                callback=self.parse_subcommand,
                meta={'subcommand': subcommand, 'command': command, 'command_docs': command_docs}
            )

    def parseSynopsis(self, synopsis):
        tokens = [
            item for sublist in
            synopsis.find_all('span')
            for item in sublist
        ]

        results = []
        state = {
            'current': [],
            'optional': False,
            'or': False,
        }

        for token in tokens:
            if (state['or']):
                state['current'].append(token)
                state['or'] = False
                continue

            if (token == '['):
                state['optional'] = True

                results.append(''.join(state['current']))
                state['current'] = []
                state['current'].append(f'{token} ')
            elif (token == ']'):
                state['optional'] = False

                state['current'].append(f' {token}')
                results.append(''.join(state['current']))
                state['current'] = []
            elif (token == '--'):
                if (state['optional']):
                    state['current'].append(token)
                    continue

                results.append(''.join(state['current']))
                state['current'] = []

                state['current'].append(token)
            elif (token == '|'):
                state['or'] = True
                state['current'].append(f' {token} ')
            elif (token == '<'):
                state['current'].append(f' {token}')
            else:
                state['current'].append(token)

        results.append(''.join(state['current']))

        del results[0]

        return list(filter(None, (result.strip() for result in results)))

    def parse_subcommand(self, response):
        command_docs = response.meta.get('command_docs')
        command = response.meta.get('command')
        subcommand = response.meta.get('subcommand')

        soup = BeautifulSoup(response.text, 'html.parser')

        args = self.parseSynopsis(soup.find(id='synopsis'))

        try:
            subcommand_docs = h.handle(str(soup.find(id=subcommand)))
        except:
            subcommand_docs = ''

        for p in soup.find(id='description').find_all('p', recursive=False):
            description = list(p.children)[0]
            try:
                json.dumps(description)
                description = h.handle(str(p))
                break
            except:
                pass

        result = {
            'cli': self.name,
            'command': command,
            'subcommand': subcommand,
            'args': args,
            'description': description.strip(),
            'meta': {
                'commandDocs': command_docs,
                'subcommandDocs': subcommand_docs,
            }
        }

        data.append(result)

        print(f'[${len(data)}] Scrapped aws {command} {subcommand}')



c = CrawlerProcess({
    'LOG_ENABLED': False
})

c.crawl(CLIScrapper)
c.start()

filename = f'scrapped/aws/aws_instructions_{time.time()}.json'

os.makedirs(os.path.dirname(filename), exist_ok=True)

with open(filename, 'w') as f:
    json.dump(data, f, indent=2)
