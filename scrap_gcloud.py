from scrapy.crawler import CrawlerProcess
import scrapy
from bs4 import BeautifulSoup
import json
import time
import os
import html2text

h = html2text.HTML2Text()
h.ignore_links = True

data = []

types = {}

urls = []


# with open('gcloud_urls/gcloud_urls.json', 'r') as f:
#     urls = json.load(f)


class CLIScrapper(scrapy.Spider):
    name = 'gcloud'
    site_url = 'cloud.google.com'
    custom_settings = {
        'TELNETCONSOLE_USERNAME': 'scrapy',
        'TELNETCONSOLE_PASSWORD': 'sc',
        'CONCURRENT_REQUESTS_PER_DOMAIN': 2,
        'CONCURRENT_REQUESTS_PER_IP': 2,
        'CONCURRENT_REQUESTS': 2,
        'CONCURRENT_ITEMS': 5,
    }

    def start_requests(self):
        if urls:
            for url in urls:
                yield scrapy.Request(
                    url=url,
                    callback=self.parse_url
                )
        else:
            root_url = 'https://cloud.google.com/sdk/gcloud/reference'
            yield scrapy.Request(callback=self.parse_root, url=root_url)

    def parse_root(self, response):
        soup = BeautifulSoup(response.text, 'html.parser')

        routes = [
            a['href']
            for ul in soup.find_all('ul', {'class': 'devsite-nav-list'}, menu='_book')
            for a in ul.find_all('a', {'class': 'devsite-nav-title'})
        ]

        del routes[-1]
        del soup

        for route in routes:
            if route.strip(' ').startswith('/sdk/gcloud/reference/beta') or route.strip(' ').startswith('/sdk/gcloud/reference/alpha'):
                continue
            url = f'https://{self.site_url}{route}'

            # urls.append(url)
            # continue

            yield scrapy.Request(
                url=url,
                callback=self.parse_url
            )

    def parse_synopsis(self, synopsis):
        chars = h.handle(str(synopsis))[1:]
        chars = chars[chars.find('`') + 2:].replace('`', '')
        return chars

    def parse_url(self, response):
        soup = BeautifulSoup(response.text, 'html.parser')

        groups = soup.find(id='GROUP')
        commands = soup.find(id='COMMAND')

        reference_path = response.url.split('/')[6:]

        # Check if the url is bottom level command
        if not commands and not groups:
            print(response.url)
            # print(reference_path)

            # try:
            #     synopsis = self.parse_synopsis(soup.find(id='SYNOPSIS').find(
            #         'span', {'class': 'normalfont'}))
            #     # print('synopsis', synopsis)
            # except:
            #     synopsis = ''

            try:
                ts = soup.find(
                    'div', {'class': 'devsite-article-body'}).find('dl', {'recursive': False})
                ts = [t['id']
                      for t in ts.find_all('section', {'recursive': False})]
                for t in ts:
                    types[t] = True
            except:
                pass

            try:
                positional_arguments = soup.find(id='POSITIONAL-ARGUMENTS')
                positional_arguments = [
                    h.handle(str(arg))
                    for arg in positional_arguments
                    .find('dd', {'class': 'sectionbody'})
                    .find_all('dt', id=lambda x: x is not None)
                ]
            except:
                positional_arguments = []

            try:
                other_flags = soup.find(id='OTHER-FLAGS')
                other_flags = [h.handle(str(arg)) for arg in other_flags.find(
                    'dd', {'class': 'sectionbody'}).find_all('dt', id=lambda x: x is not None)]
            except:
                other_flags = []

            try:
                common_flags = soup.find(id='COMMONLY-USED-FLAGS')
                common_flags = [h.handle(str(arg)) for arg in common_flags.find(
                    'dd', {'class': 'sectionbody'}).find_all('dt', id=lambda x: x is not None)]
            except:
                list_flags = []

            try:
                list_flags = soup.find(id='LIST-COMMAND-FLAGS')
                list_flags = [h.handle(str(arg)) for arg in list_flags.find(
                    'dd', {'class': 'sectionbody'}).find_all('dt', id=lambda x: x is not None)]
            except:
                list_flags = []

            try:
                flags = soup.find(id='FLAGS')
                flags = [h.handle(str(arg)) for arg in flags.find(
                    'dd', {'class': 'sectionbody'}).find_all('dt', id=lambda x: x is not None)]
            except:
                flags = []

            try:
                gcloud_wide_flags = True if bool(
                    soup.find(id='GCLOUD-WIDE-FLAGS')) else False
            except:
                gcloud_wide_flags = False

            try:
                required_flags = soup.find(id='REQUIRED-FLAGS')
                required_flags = [h.handle(str(arg)) for arg in required_flags.find(
                    'dd', {'class': 'sectionbody'}).find_all('dt', id=lambda x: x is not None)]
            except:
                required_flags = []

            try:
                optional_flags = soup.find(id='OPTIONAL-FLAGS')
                optional_flags = [h.handle(str(arg)) for arg in optional_flags.find(
                    'dd', {'class': 'sectionbody'}).find_all('dt', id=lambda x: x is not None)]
            except:
                optional_flags = []

            try:
                description = h.handle(str(soup.find(id='DESCRIPTION').find(
                    'dd', {'class': 'sectionbody'})))
                # print('description', description)
            except:
                description = ''

            try:
                docs = h.handle(
                    str(soup.find('div', {'class': 'devsite-article-body'})))
            except:
                docs = ''

            c = {
                'cli': self.name,
                'commands': [
                    *[
                        {
                            'value': command,
                        } for command in reference_path[:-1]
                    ],
                    {
                        'value': reference_path[-1],
                        'isCommand': True,
                        'docs': docs,
                    },
                ],
                'description': description,
                'args': {
                    'requiredFlags': required_flags,
                    'globalFlags': gcloud_wide_flags,
                    'optionalFlags': optional_flags,
                    'flags': flags,
                    'otherFlags': other_flags,
                    'listFlags': list_flags,
                    'commonFlags': common_flags,
                    'positionalArguments': positional_arguments,
                },

            }

            data.append(c)
            print(f'[{len(data)}]')


c = CrawlerProcess({
    'LOG_ENABLED': False
})

c.crawl(CLIScrapper)
c.start()

filename = f'scrapped/gcloud/gcloud_instructions_{time.time()}.json'

os.makedirs(os.path.dirname(filename), exist_ok=True)

# with open('gcloud_urls.json', 'w') as f:
#     json.dump(urls, f, indent=2)

with open(filename, 'w') as f:
    json.dump(data, f, indent=2)

with open('gcloud_types.json', 'w') as f:
    json.dump(types, f, indent=2)
