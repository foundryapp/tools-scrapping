from scrapy.crawler import CrawlerProcess
import scrapy
from bs4 import BeautifulSoup
import json
import time
import os
import html2text
from functools import reduce  # forward compatibility for Python 3
import operator


h = html2text.HTML2Text()
h.ignore_links = True

data = []

counter = 0

with open('gcloud_instructions.json', 'r') as f:
    gcloud_rawlines = [' '.join([c['value']
                                 for c in i['commands']]) for i in json.load(f)]


class CLIScrapper(scrapy.Spider):
    name = 'gcloud'
    site_url = 'cloud.google.com'
    counter = 0

    custom_settings = {
        'TELNETCONSOLE_USERNAME': 'scrapy',
        'TELNETCONSOLE_PASSWORD': 'sc',
        'CONCURRENT_REQUESTS_PER_DOMAIN': 2,
        'CONCURRENT_REQUESTS_PER_IP': 2,
        'CONCURRENT_REQUESTS': 2,
        'CONCURRENT_ITEMS': 5,
    }

    def start_requests(self):
        root_url = 'https://cloud.google.com/sdk/gcloud/reference'
        yield scrapy.Request(callback=self.parse_root, url=root_url)

    def parse_root(self, response):
        soup = BeautifulSoup(response.text, 'html.parser')

        routes = [
            a['href']
            for ul in soup.find_all('ul', {'class': 'devsite-nav-list'}, menu='_book')
            for a in ul.find_all('a', {'class': 'devsite-nav-title'})
        ]

        del routes[-1]
        del soup

        for route in routes:
            if route.strip(' ').startswith('/sdk/gcloud/reference/beta') or route.strip(' ').startswith('/sdk/gcloud/reference/alpha'):
                continue
            url = f'https://{self.site_url}{route}'
            reference_path = route.split('/')[4:]
            if ' '.join(reference_path) in gcloud_rawlines:
                continue

            print(route)

            yield scrapy.Request(
                url=url,
                callback=self.parse_url
            )

    def parse_synopsis(self, synopsis):
        chars = h.handle(str(synopsis))[1:]
        chars = chars[chars.find('`') + 2:].replace('`', '')
        return chars

    def parse_url(self, response):
        soup = BeautifulSoup(response.text, 'html.parser')

        groups = soup.find(id='GROUP')
        commands = soup.find(id='COMMAND')

        reference_path = response.url.split('/')[6:]

        # print(response.url)
        # Check if the url is bottom level command
        if commands or groups:
            print(reference_path)

            try:
                soup.find(id='GROUP').decompose()
            except:
                pass
            try:
                soup.find(id='COMMAND').decompose()
            except:
                pass

            try:
                commands = soup.find(id='COMMAND')
                docs = h.handle(
                    str(soup.find('div', {'class': 'devsite-article-body'})))
            except:
                docs = ''

            self.counter += 1
            print(f'{self.counter}')

            data.append({
                'docs_': docs,
                'reference_path': reference_path,
            })


c = CrawlerProcess({
    'LOG_ENABLED': False
})

c.crawl(CLIScrapper)
c.start()

filename = f'scrapped/gcloud/docs/gcloud_group_docs_{time.time()}.json'

os.makedirs(os.path.dirname(filename), exist_ok=True)


joined_data = {}


def getFromDict(dataDict, mapList):
    return reduce(operator.getitem, mapList, dataDict)

def setInDict(dataDict, mapList, value):
    getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value

def nested_set(dic, keys, value):
    for key in keys[:-1]:
        dic = dic.setdefault(key, {})
    dic[keys[-1]] = value

for doc in data:
    try:
        command = getFromDict(joined_data, doc['reference_path'])
    except:
        command = {}

    print(doc)
    nested_set(joined_data, doc['reference_path'], {**command, 'docs_': doc['docs_']})


with open(filename, 'w') as f:
    json.dump(joined_data, f, indent=2)
